QT       += network webkit xml xmlpatterns

TARGET = videofeed
TEMPLATE = app


SOURCES += main.cpp \
        mainwindow.cpp

HEADERS  += mainwindow.h

RESOURCES += teddemo.qrc

FORMS    += mainwindow.ui
